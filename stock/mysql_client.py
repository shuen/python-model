#!/usr/bin/env python
# -*- coding: utf8 -*-
import pymysql
import subprocess

mysql_connect = pymysql.connect(host='localhost', user='root', passwd='123456aA.', port=3306, db='stock',
                                charset='utf8')
cur = mysql_connect.cursor()
cur.execute("show global status")
res = list(cur.fetchall())
# print(res)
mysql_dict = {}

for i in res:
    if list(i)[1].strip() == '':
        # print('列表第二个元素为空')
        pass
    else:
        tmp_dict = {list(i)[0]: list(i)[1]}
        # print('分割线------------------------')
        mysql_dict.update(tmp_dict)

print(mysql_dict["Connections"])
cur.close()
mysql_connect.close()
