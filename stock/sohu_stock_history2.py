import pandas as pd
import os


def read_stock():
    years = [2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022]
    stock = pd.read_csv('D:/data/stock/A-share.csv', encoding='utf8', converters={'SYMBOL': str})
    for code in stock.SYMBOL:

        for year in years:
            path = 'D:/data/stock/%s_%s.csv' % (code, year)
            stock_year = pd.read_csv(path, index_col=0)

            os.remove(path)
