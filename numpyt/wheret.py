'''
Created on 2018年6月23日

@author: Administrator
'''
import numpy as np

t1 = np.where([[True, False], [True, True]], [[1, 2], [3, 4]], [[9, 8], [7, 6]])
print(t1)

t2 = np.where([[0, 1], [1, 0]])
print(t2)

x = np.arange(9.).reshape(3, 3)
t3 = np.where(x > 5)
print(t3)

t4 = np.where(x < 5, x, -1)
print(t4)

goodvalues = [7, 8, 9]
ix = np.isin(x, goodvalues)
print(ix)
t5 = np.where(ix)
print(t5)
