import copy

# 直接地址引用
a = [1, 2, 3]
b = a
b[1] = 22
print(id(a) == id(b))
print(id(a[1]) == id(b[1]))

# deep copy
c = copy.deepcopy(a)
c[1] = 2
a[1] = 111
print(id(a) == id(c))
print(id(a[1]) == id(c[1]))

# shallow copy
a = [1, 2, [3, 4]]
d = copy.copy(a)
print(id(a) == id(d))
print(id(a[2]) == id(d[2]))