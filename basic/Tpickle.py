import pickle
# 对象序列化
a_dict = {'da': 111, 2: [23, 1, 4], '23': {1: 2, 'd': 'sad'}}

# pickle a variable to a file
file = open('objectfile', 'wb')
# file = open('pickle_example.pickle', 'wb')
pickle.dump(a_dict, file)
file.close()

# reload a file to a variable
with open('objectfile', 'rb') as file:
    a_dict1 = pickle.load(file)

print(a_dict1)
