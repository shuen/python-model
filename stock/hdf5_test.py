import pandas as pd
import os

PATH_TO_CSV = 'D:/data/stock/hdf5_test.csv'

ROOT_H5_1MIN = 'D:/data/stock/'

stock_code = '0000001'

columns = ['date', 'time', 'open', 'high', 'low', 'close', 'volume', 'amount']
df = pd.read_csv(PATH_TO_CSV, header=None, names=columns)
# df['datetime'] = pd.to_datetime(df.date + ' ' + df.time, format='%Y-%m-%d %H:%M:%S')  # 将数据类型转换成时间类型
# df.set_index('datetime', inplace=True)  # 将date设置为index
# df.drop(['date', 'time'], axis=1, inplace=True)
dest_file = os.path.join(ROOT_H5_1MIN, stock_code + '.h5')
df.to_hdf(dest_file, key='data', mode='w', format='table')

