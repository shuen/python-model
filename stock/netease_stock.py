"""
网易财经
"""

import json
import requests
import pandas as pd


def build_stock(path='D:/data/stock/A-share.csv', home_page=0):
    """
    Obtain all A-share interfaces of NetEase

    Parameters
    -------
    path : str
        save file path

    home_page : int
        Download the opening page number

    Returns
    -------
    i : int
        Download progress
    """
    df = pd.read_csv(path)
    try:
        url = 'http://quotes.money.163.com/hs/service/diyrank.php?&page=%s&query=STYPE:EQA&fields=NO,SYMBOL,NAME&count=24&type=query'
        response = requests.get(url % home_page)
        content = json.loads(response.content)
        pagecount = content.get('pagecount')
        print('total stock: ', pagecount)
        df = pd.DataFrame(content.get('list'), columns=['NAME', 'SYMBOL'])
        if pagecount-1 == home_page:
            return 0
        for i in range(home_page + 1, pagecount):
            print('Download progress: ', i)
            content = json.loads(requests.get(url % i).content)
            df = df.append(pd.DataFrame(content.get('list'), columns=['NAME', 'SYMBOL']), ignore_index=True)

        print('Download completes')
    except Exception as e:
        print(e)
    finally:
        df = df.drop_duplicates(subset='SYMBOL')
        df.to_csv(path)
    return i


if __name__ == '__main__':
    build_stock()

