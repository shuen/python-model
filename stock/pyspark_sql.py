from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.functions import substring
from pyspark.sql import functions
import time

import tensorboard

import torch

conf = SparkConf().setMaster('local[*]').setAppName('commend_keyword')
sc = SparkContext(conf=conf)
#
sqlContext = SQLContext(sc)


select_columns = """
                    (select * from user) t
                 """

# 通过sqlContext读取mysql中指定表的数据
data = sqlContext.read.format("jdbc").options(url="jdbc:mysql://localhost:3306/stock?"
                                                  "useUnicode=true&characterEncoding=utf8&allowMultiQueries=true"
                                                  "&zeroDateTimeBehavior=convertToNull",
                                              driver="com.mysql.jdbc.Driver", dbtable=select_columns,
                                              user="root", password="123456aA.").load()

print(data)
