# import findspark
# findspark.init()

from pyspark import SparkConf, SparkContext

if __name__ == '__main__':
    conf = SparkConf().setMaster('local[*]').setAppName('map')
    sc = SparkContext(conf=conf)

    rdd1 = sc.parallelize([1, 2, 3, 4, 5]).map(lambda x: x + 1)
    print(rdd1.collect())

    rdd2 = sc.parallelize(["dog", "tiger", "cat", "tiger", "tiger", "cat"]).map(lambda x: (x, 1)).reduceByKey(
        lambda x, y: x + y)
    print(rdd2.collect())

    sc.stop()