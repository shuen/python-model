a = [1, 2, 3]
b = [4, 5, 6]

ls = list(zip(a, a, b))

for k in ls:
    print(k)


for i, j in zip(a, b):
    print(i, j)


def f2(x, y): return x + y


# for map
print(list(map(f2, [5, 3], [4, 5])))
