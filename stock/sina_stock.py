import requests


def get_tick():
    """
    获取某股票最新成交价
    :return:
      股票名称、今日开盘价、昨日收盘价、当前价格、今日最高价、今日最低价、竞买价、竞卖价、成交股数、成交金额、买1手、买1报价、买2手、买2报价、…、买5报价、…、卖5报价、日期、时间
    """
    url = "http://hq.sinajs.cn/list=sh600519"
    headers = {'Referer': 'https://finance.sina.com.cn/'}
    page = requests.get(url, headers=headers)

    stock_info = page.text
    print(stock_info)
    mt_info = stock_info.split(",")  # 爬取到数据信息

    last = float(mt_info[1])
    trade_datetime = mt_info[30] + ' ' + mt_info[31]  # 交易时间

    tick = (last, trade_datetime)

    return tick  # 得到股票最新成交价、时间


last_tick = get_tick()  # 进入网页获取数据
print(last_tick)

"""
获取各个时间段行情图
日K线图: http://image.sinajs.cn/newchart/daily/n/sh601006.gif
分时线的查询: http://image.sinajs.cn/newchart/min/n/sh000001.gif
日K线查询: http://image.sinajs.cn/newchart/daily/n/sh000001.gif
周K线查询: http://image.sinajs.cn/newchart/weekly/n/sh000001.gif
月K线查询: http://image.sinajs.cn/newchart/monthly/n/sh000001.gif
"""

"""

http://money.finance.sina.com.cn/quotes_service/api/json_v2.php/CN_MarketData.getKLineData?symbol=[市场][股票代码]&scale=[周期]&ma=no&datalen=[长度]
返回结果：获取5、10、30、60分钟JSON数据；day日期、open开盘价、high最高价、low最低价、close收盘价、volume成交量；向前复权的数据。
pass:
http://money.finance.sina.com.cn/quotes_service/api/json_v2.php/CN_MarketData.getKLineData?symbol=sz002095&scale=60&ma=no&datalen=1023
"""

"""
http://finance.sina.com.cn/realstock/company/[市场][股票代码]/[复权].js?d=[日期]
"""



