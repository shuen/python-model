import re

# matching string
pattern1 = "dog"
string = "dog runs to cat"
ptn = r"r[au]n"       # start with "r" means raw string

print(re.search(pattern1, string))
print(re.search(ptn, "dog runs to cat"))
