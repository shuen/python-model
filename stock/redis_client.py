import redis

# !/usr/bin/env python
import redis

redis_dict = {}
conn = redis.Redis(host='localhost', port=6379, password="")
redis_dict = conn.info()  # 输出redis的info信息，以字典的形式输出
print(redis_dict)
print('+++++++++++++++++++')
print(conn.client_list())  # 输出连接redis的客户端信息，以字典形式输出
